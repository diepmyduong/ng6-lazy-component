import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Ng6LazyComponentModule } from 'ng6-lazy-component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    Ng6LazyComponentModule.forRoot([
      { componentId: 'SampleA', path: 'SampleA', loadChildren: './components/sample-a/sample-a.module#SampleAModule' }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
