import { NgModule, ModuleWithProviders, NgModuleFactoryLoader, SystemJsNgModuleLoader, InjectionToken } from '@angular/core';
import { Ng6LazyComponentComponent } from './ng6-lazy-component.component';
import { LazyComponentManifest, LAZY_COMPONENT_MANIFESTS } from './lazy-component.manifest';
import { ROUTES } from '@angular/router';
import { Ng6LazyComponentService } from './ng6-lazy-component.service';
@NgModule({
  imports: [
  ],
  declarations: [Ng6LazyComponentComponent],
  exports: [Ng6LazyComponentComponent],
  providers: [{ provide: NgModuleFactoryLoader, useClass: SystemJsNgModuleLoader }]
})
export class Ng6LazyComponentModule {
  static forRoot(manifests: LazyComponentManifest[]): ModuleWithProviders {
    return {
      ngModule: Ng6LazyComponentModule,
      providers: [
        // provider for Angular CLI to analyze
        { provide: ROUTES, useValue: manifests, multi: true },
        { provide: LAZY_COMPONENT_MANIFESTS, useValue: manifests },
        Ng6LazyComponentService
      ],
    };
  }
}

