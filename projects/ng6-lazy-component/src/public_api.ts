/*
 * Public API Surface of ng6-lazy-component
 */

export * from './lib/ng6-lazy-component.service';
export * from './lib/ng6-lazy-component.component';
export * from './lib/ng6-lazy-component.module';
export * from './lib/lazy-component.manifest';