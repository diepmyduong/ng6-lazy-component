import { InjectionToken } from "@angular/core";

export interface LazyComponentManifest {
    componentId: string;
    path: string;
    loadChildren: string;
}

export const LAZY_COMPONENT_MANIFESTS = new InjectionToken<any>(`LAZY_COMPONENT_MANIFESTS`);