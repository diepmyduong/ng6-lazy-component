import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ng6LazyComponentComponent } from './ng6-lazy-component.component';

describe('Ng6LazyComponentComponent', () => {
  let component: Ng6LazyComponentComponent;
  let fixture: ComponentFixture<Ng6LazyComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ng6LazyComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ng6LazyComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
