# ng6-lazy-component

## Introduction

> A simple module make component lazy load without import it to module. Make your module scalable.

## Code Samples

```html
// path: /app/app.compoment.html
<lzc id="SampleA"></lzc>
```
```javascript
// path: app/app.module.ts
import { Ng6LazyComponentModule } from 'ng6-lazy-component';
...
@NgModule({
...
imports: [
    BrowserModule,
    Ng6LazyComponentModule.forRoot([
      { componentId: 'SampleA', path: 'SampleA', loadChildren: './components/sample-a/sample-a.module#SampleAModule' }
    ])
  ],

// path: components/sample-a/sample-a.module.ts
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SampleAComponent } from './sample-a.component';
import { LAZY_COMPONENT } from 'ng6-lazy-component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SampleAComponent],
  entryComponents: [SampleAComponent],
  providers: [{ provide: LAZY_COMPONENT, useValue: SampleAComponent }]
})
export class SampleAModule { }
```
### Runtime use
```html
<ng-template #lazyComponent></ng-template>
```
```javascript
@ViewChild("lazyComponent", { read: ViewContainerRef }) lazyComponentRef: ViewContainerRef;
constructor(private lazyLoader: Ng6LazyComponentService) { }
ngAfterViewInit() {
    this.lazyLoader
    .getComponentFactory("SampleA") // run this function will download component module
    .then(factory => { 
        // If you need render component in runtime
        const ref = this.lazyComponentRef.createComponent(factory);
    })
  }
```


## Installation

>  npm i ng6-lazy-component