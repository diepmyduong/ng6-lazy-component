import { TestBed, inject } from '@angular/core/testing';

import { Ng6LazyComponentService } from './ng6-lazy-component.service';

describe('Ng6LazyComponentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Ng6LazyComponentService]
    });
  });

  it('should be created', inject([Ng6LazyComponentService], (service: Ng6LazyComponentService) => {
    expect(service).toBeTruthy();
  }));
});
