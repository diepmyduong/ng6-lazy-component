import { Injectable, Inject, NgModuleFactoryLoader, Injector, ComponentFactory, InjectionToken } from '@angular/core';
import { LAZY_COMPONENT_MANIFESTS, LazyComponentManifest } from './lazy-component.manifest';

@Injectable({
  providedIn: 'root'
})
export class Ng6LazyComponentService {

  constructor(
    @Inject(LAZY_COMPONENT_MANIFESTS) private manifests: LazyComponentManifest[],
    private loader: NgModuleFactoryLoader,
    private injector: Injector
  ) { 
    console.log('manifests', manifests)
  }


  async getComponentFactory<T>(componentId: string, injector?: Injector): Promise<ComponentFactory<T>> {
    const manifest = this.manifests
      .find(m => m.componentId === componentId);

    const p = this.loader.load(manifest.loadChildren)
      .then(ngModuleFactory => {
        const moduleRef = ngModuleFactory.create(injector || this.injector);

        // Read from the moduleRef injector and locate the dynamic component type
        const dynamicComponentType = moduleRef.injector.get(LAZY_COMPONENT);
        // Resolve this component factory
        return moduleRef.componentFactoryResolver.resolveComponentFactory<T>(dynamicComponentType);
      });

    return p;
  }
}

export const LAZY_COMPONENT = new InjectionToken<any>('LAZY_COMPONENT');