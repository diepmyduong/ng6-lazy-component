import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SampleAComponent } from './sample-a.component';
import { LAZY_COMPONENT } from 'ng6-lazy-component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SampleAComponent],
  entryComponents: [SampleAComponent],
  providers: [{ provide: LAZY_COMPONENT, useValue: SampleAComponent }]
})
export class SampleAModule { }
