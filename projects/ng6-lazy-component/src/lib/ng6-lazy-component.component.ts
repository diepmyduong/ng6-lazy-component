import { Component, OnInit, Input, Output, ViewChild, EventEmitter, ViewContainerRef } from '@angular/core';
import { Ng6LazyComponentService } from './ng6-lazy-component.service';

@Component({
  selector: 'lzc',
  template: `<ng-template #lazyContainer></ng-template>`,
  styles: []
})
export class Ng6LazyComponentComponent implements OnInit {
  @Input() id: string;
  @Output() loaded = new EventEmitter<any>();
  @ViewChild("lazyContainer", { read: ViewContainerRef }) lazyContainerRef: ViewContainerRef;
  constructor(
    private dynamicComponentLoader: Ng6LazyComponentService
  ) { }
  ngOnInit() { }

  ngAfterViewInit() {
    this.dynamicComponentLoader.getComponentFactory(this.id).then(factory => {
      const ref = this.lazyContainerRef.createComponent(factory);
      this.loaded.emit(ref);
    })
  }

}
